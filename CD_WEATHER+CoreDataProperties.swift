//
//  CD_WEATHER+CoreDataProperties.swift
//  aws
//
//  Created by Shepherd on 2016. 5. 15..
//  Copyright © 2016년 Shepherd. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension CD_WEATHER {

    @NSManaged var cityName: String?
    @NSManaged var humidity: String?
    @NSManaged var image: NSData?
    @NSManaged var observationTime: String?
    @NSManaged var weatherDescription: String?
    @NSManaged var saveDate: NSDate?

}
