//
//  WeatherViewControllerTests.swift
//  aws
//
//  Created by Shepherd on 2016. 5. 15..
//  Copyright © 2016년 Shepherd. All rights reserved.
//

import XCTest
@testable import aws

class WeatherViewControllerTests: XCTestCase {
    
    var viewController : WeatherResultViewController?
    
    override func setUp() {
        super.setUp()
        
        viewController = WeatherResultViewController()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAWSViewControllerHasTableView() {
        XCTAssertNotNil(viewController?.mainTableView)
        //        XCTAssert( AWSViewController .conformsToProtocol(UITableViewDataSource))
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
