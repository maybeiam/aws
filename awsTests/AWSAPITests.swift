//
//  AWSAPITests.swift
//  aws
//
//  Created by Shepherd on 2016. 5. 15..
//  Copyright © 2016년 Shepherd. All rights reserved.
//

import XCTest
@testable import aws

class AWSAPITests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testWeatherAPIResponde() {
        let apiResponseExpectation = self.expectationWithDescription("apiResponded")
        
        HttpRequest.requestGet(BASE_URL, targetUrl: RETREIVE_WEATHERLIST, success: { (response) -> Void in
            let responseInDictionary = response as? NSDictionary
                if responseInDictionary == nil {
                    XCTAssert(false)
                } else {
                    XCTAssert(true)
                }
            
                apiResponseExpectation.fulfill()
            }, failure: { (error) -> Void in
                XCTAssert(false)
                apiResponseExpectation.fulfill()
            }, parametersObjectsAndKeys: "dd5bc7742928d1633213b5da075ec", "key", "London", "q", "yes", "fx", "json", "format", "24", "tp", "1", "num_of_days")
        
        self.waitForExpectationsWithTimeout(20.0) { (error) in
            
        }
    }
    
    func testWeatherAPIHasData() {
        let apiResponseExpectation = self.expectationWithDescription("apiResponded")
        
        HttpRequest.requestGet(BASE_URL, targetUrl: RETREIVE_WEATHERLIST, success: { (response) -> Void in
            let responseInDictionary = response as? NSDictionary
            if responseInDictionary == nil {
                XCTAssert(false)
            } else {
                if responseInDictionary?.objectForKey("data") == nil {
                    XCTAssert(false)
                } else {
                    XCTAssert(true)
                }
            }
            
            apiResponseExpectation.fulfill()
            }, failure: { (error) -> Void in
                XCTAssert(false)
                apiResponseExpectation.fulfill()
            }, parametersObjectsAndKeys: "dd5bc7742928d1633213b5da075ec", "key", "London", "q", "yes", "fx", "json", "format", "24", "tp", "1", "num_of_days")
        
        self.waitForExpectationsWithTimeout(20.0) { (error) in
            
        }
    }
    
    func testWeatherAPICurrent_ConditionInDataIsArray() {
        let apiResponseExpectation = self.expectationWithDescription("apiResponded")
        
        HttpRequest.requestGet(BASE_URL, targetUrl: RETREIVE_WEATHERLIST, success: { (response) -> Void in
            let responseInDictionary = response as? NSDictionary
            let data = responseInDictionary?.objectForKey("data") as? NSDictionary
            if data == nil {
                XCTAssert(false)
            } else {
                let current_condition = data?.objectForKey("current_condition")
                if current_condition!.isKindOfClass(NSArray) {
                    XCTAssert(true)
                } else {
                    XCTAssert(false)
                }
            }
            
            apiResponseExpectation.fulfill()
            }, failure: { (error) -> Void in
                XCTAssert(false)
                apiResponseExpectation.fulfill()
            }, parametersObjectsAndKeys: "dd5bc7742928d1633213b5da075ec", "key", "London", "q", "yes", "fx", "json", "format", "24", "tp", "1", "num_of_days")
        
        self.waitForExpectationsWithTimeout(20.0) { (error) in
            
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
