//
//  WeatherResultViewController.swift
//  aws
//
//  Created by Shepherd on 2016. 5. 15..
//  Copyright © 2016년 Shepherd. All rights reserved.
//

import UIKit

class WeatherResultViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    let WeatherResultTableViewCellId = "WeatherResultTableViewCellId"
    let mainTableView = UITableView()
    var refreshControl = UIRefreshControl()
    var weathers : LIST?
    var api_key : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        // Do any additional setup after loading the view.
    }
    
    func initViews() {
        self.mainTableView.delegate = self;
        self.mainTableView.dataSource = self;
        self.mainTableView.frame = CGRectMake(0.0, 44.0 + 20.0, SCREEN_WIDTH, SCREEN_HEIGHT)
        self.view.addSubview(self.mainTableView)
        mainTableView.registerNib(UINib(nibName: "WeatherResultTableViewCell", bundle: nil), forCellReuseIdentifier: WeatherResultTableViewCellId )
        
        refreshControl.addTarget(self, action: #selector(WeatherResultViewController.pullToRefresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        mainTableView.addSubview(refreshControl)
    }

    func pullToRefresh(sender:AnyObject) {
        if Reachability.isConnectedToNetwork() == false {
            self.refreshControl.endRefreshing()
            return ;
        }
        
        if weathers!.datas!.count <= 0 {
            return ;
        } else {
            let weather = weathers!.datas?.objectAtIndex(0) as! WEATHER
            let query = weather.city
            
            HttpRequest.requestGet(BASE_URL, targetUrl: RETREIVE_WEATHERLIST, success: { (response) -> Void in
                self.refreshControl.endRefreshing()
                let responseInDictionary = response as? NSDictionary
                if responseInDictionary == nil {
                    
                } else {
                    self.weathers = LIST(weather: responseInDictionary)
                    dispatch_async(dispatch_get_main_queue(), { 
                        self.mainTableView.reloadData()
                    })
                    
                }
                
                }, failure: { (error) -> Void in
                    self.refreshControl.endRefreshing()
                }, parametersObjectsAndKeys: api_key!, "key", query!, "q", "yes", "fx", "json", "format", "24", "tp", "1", "num_of_days")
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weathers == nil ? 0 : weathers!.datas!.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(WeatherResultTableViewCellId ) as? WeatherResultTableViewCell
        let weatherInfo = weathers!.datas?.objectAtIndex(indexPath.row) as? WEATHER
        cell?.humidityLabel.text = (weatherInfo?.humidity)! + "%"
        cell?.cityNameLabel.text = weatherInfo?.city
        cell?.observationTimeLabel.text = weatherInfo?.observation_time
        cell?.weatherDescriptionLabel.text = weatherInfo?.weatherDesc
        
        if weatherInfo?.weatherIconImage?.image == nil  {
            cell?.iconImageView.image = nil
            if weatherInfo?.weatherIconImage?.url != nil {
                HttpRequest.requestImage((weatherInfo?.weatherIconImage?.url)!, success: { (image) in
                    weatherInfo?.weatherIconImage!.image = image
                    // save image data into cd_weather
                    DataManager.sharedInstance.updateImageWithWeather(weatherInfo!)
                        let currentCell = self.mainTableView.cellForRowAtIndexPath(indexPath) as? WeatherResultTableViewCell
                    dispatch_async(dispatch_get_main_queue(), { 
                        currentCell?.iconImageView.image = weatherInfo?.weatherIconImage!.image
                    })
                    
                    
                    }, failure: { (error) in
                        
                })
            }
        }
        else {
            cell!.iconImageView.image = weatherInfo?.weatherIconImage!.image
        }
        
        return cell!
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 74.0
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
