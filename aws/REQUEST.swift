//
//  REQUEST.swift
//  aws
//
//  Created by Shepherd on 2016. 5. 15..
//  Copyright © 2016년 Shepherd. All rights reserved.
//

import UIKit

class REQUEST: NSObject {
    var query : String?
    var type : String?
    
    init( dictionary : NSDictionary? ) {
        query = dictionary?.objectForKey("query") as? String
        type = dictionary?.objectForKey("type") as? String
    }
    
    init( cd_weather : CD_WEATHER? ) {
        query = cd_weather?.cityName
        type = "City"
    }
}
