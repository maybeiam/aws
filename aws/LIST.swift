//
//  LIST.swift
//  aws
//
//  Created by Shepherd on 2016. 5. 15..
//  Copyright © 2016년 Shepherd. All rights reserved.
//

import UIKit

class LIST: NSObject {
    var datas : NSMutableArray?
    
    init( weather : NSDictionary? ) {
        datas = NSMutableArray()
        let data = weather?.objectForKey("data") as? NSDictionary
        let current_condition = data?.objectForKey("current_condition") as? NSArray
        for dictionary in (current_condition! as NSArray as! [NSDictionary]) {
            let weather = WEATHER(dictionary: dictionary)
            let requests = data?.objectForKey("request") as? NSArray
            let request = requests?.objectAtIndex(0) as? NSDictionary

            let processedRequest = REQUEST(dictionary: request)
            weather.request = processedRequest
            datas?.addObject(weather)
        }
    }
    
    init( cd_weather : CD_WEATHER ) {
        datas = NSMutableArray()
        let weather = WEATHER( cd_weather : cd_weather )
        datas?.addObject(weather)
    }
}
