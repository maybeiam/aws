//
//  DataManager.swift
//  aws
//
//  Created by Shepherd on 2016. 5. 15..
//  Copyright © 2016년 Shepherd. All rights reserved.
//

import UIKit
import CoreData

class DataManager: NSObject {
    static let sharedInstance = DataManager()
    let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    func fetchAllAvaiableWeatherData() -> NSArray? {
        let fetchRequest = NSFetchRequest()
        let entity = NSEntityDescription.entityForName("CD_WEATHER", inManagedObjectContext: context)
        fetchRequest.entity = entity
        
        do {
            let fetchedArray = try context.executeFetchRequest(fetchRequest)
            return fetchedArray
        } catch {
            return nil
        }
    }
    
    func getWeatherData( cityName : String ) -> CD_WEATHER? {
        let fetchRequest = NSFetchRequest()
        let entity = NSEntityDescription.entityForName("CD_WEATHER", inManagedObjectContext: context)
        fetchRequest.entity = entity
        
        let predicate = NSPredicate(format: "cityName == %@", cityName )
        fetchRequest.predicate = predicate
        
        do {
            let fetchedArray = try context.executeFetchRequest(fetchRequest)
            
            if fetchedArray.count > 0 {
                return fetchedArray[0] as? CD_WEATHER
            } else {
                return nil
            }
        } catch {
            return nil
        }
    }
    
    func createWeatherData() ->CD_WEATHER {
        let weather = NSEntityDescription.insertNewObjectForEntityForName("CD_WEATHER", inManagedObjectContext: context) as! CD_WEATHER
        weather.saveDate = NSDate()
        return weather
    }
    
    func createWeatherDataWithWeather( weatherData : WEATHER ) -> CD_WEATHER {
        let weather = NSEntityDescription.insertNewObjectForEntityForName("CD_WEATHER", inManagedObjectContext: context) as! CD_WEATHER
        weather.saveDate = NSDate()
        weather.cityName = weatherData.city
        weather.humidity = weatherData.humidity
        weather.observationTime = weatherData.observation_time
        weather.weatherDescription = weatherData.weatherDesc
        if weatherData.weatherIconImage!.image != nil {
            weather.image = UIImagePNGRepresentation(weatherData.weatherIconImage!.image!)
        }
        return weather
    }
    
    func updateImageWithWeather( weatherData : WEATHER ) {
        let fetchRequest = NSFetchRequest()
        let entity = NSEntityDescription.entityForName("CD_WEATHER", inManagedObjectContext: context)
        fetchRequest.entity = entity
        
        let predicate = NSPredicate(format: "cityName == %@", weatherData.city! )
        fetchRequest.predicate = predicate
        
        do {
            let fetchedArray = try context.executeFetchRequest(fetchRequest)
            
            if fetchedArray.count > 0 {
                let weather = fetchedArray[0] as! CD_WEATHER
                weather.image = UIImagePNGRepresentation(weatherData.weatherIconImage!.image!)
                self.save()
            } else {
                
            }
        } catch {
            
        }
    }
    
    func insertSearchData( newValue : WEATHER ) -> Bool {
        let fetchRequest = NSFetchRequest()
        let entity = NSEntityDescription.entityForName("CD_WEATHER", inManagedObjectContext: context)
        fetchRequest.entity = entity
        let entityCount = context.countForFetchRequest(fetchRequest, error: nil)
        
        let predicate = NSPredicate(format: "cityName == %@", newValue.city! )
        fetchRequest.predicate = predicate
        
        do {
            let fetchedArray = try context.executeFetchRequest(fetchRequest)
            
            if fetchedArray.count > 0 {
                // do nothing, exist
                return false
            } else {
                // save new thing
                if entityCount >= 10 {
                    let lastWeather = self.getLastSavedWeather()
                    self.removeEntity(lastWeather!)
                }
                self.createWeatherDataWithWeather(newValue)
                
                self.save()
                
                return true
            }
        } catch {
            return false
        }
    }
    
    func getLastSavedWeather() -> CD_WEATHER? {
        let fetchRequest = NSFetchRequest()
        let entity = NSEntityDescription.entityForName("CD_WEATHER", inManagedObjectContext: context)
        fetchRequest.entity = entity
        
        let sortDescriptor = NSSortDescriptor(key: "saveDate", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        do {
            let fetchedArray = try context.executeFetchRequest(fetchRequest)
            
            if fetchedArray.count > 0 {
                return fetchedArray.last as? CD_WEATHER
            } else {
                return nil
            }
        } catch {
            
        }
        return nil
    }
    
    func removeEntity( entity : NSManagedObject ) {
        context.deleteObject(context.objectWithID(entity.objectID))
    }
    
    func save() {
        do {
            try context.save()

        } catch {
            
        }
    }
}
