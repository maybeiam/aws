//
//  AWSViewController.swift
//  aws
//
//  Created by Shepherd on 2016. 5. 14..
//  Copyright © 2016년 Shepherd. All rights reserved.
//

import UIKit

class AWSViewController: BaseViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    let SuggestionTableViewCellId = "SuggestionTableViewCellId"
    
    let suggestionTableView = UITableView()
    let searchView = UIView()
    let searchTextField = UITextField()
    
    var targetUrl : String?
    var api_key : String?
    
    var weatherInfos : LIST?
    var suggestionWeatherInfos : NSArray?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.retreiveData()
        self.initViews()
        // Do any additional setup after loading the view.

    }
    
    func retreiveData() {
        suggestionWeatherInfos = DataManager.sharedInstance.fetchAllAvaiableWeatherData()
        suggestionTableView.reloadData()
    }
    
    func initViews() {
        searchView.frame = CGRectMake(5.0, 44.0 + 25.0, SCREEN_WIDTH - 10.0, 50.0)
        searchView.layer.borderColor = UIColor.lightGrayColor().CGColor
        searchView.layer.borderWidth = 1.0
        searchView.layer.cornerRadius = 5.0
        self.view.addSubview(searchView)
        
        searchTextField.frame = CGRectMake(searchView.frame.origin.x + 5.0, searchView.frame.origin.y, searchView.frame.size.width - 10.0, searchView.frame.size.height )
        searchTextField.returnKeyType = UIReturnKeyType.Search
        searchTextField.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
        searchTextField.delegate = self;
        self.view.addSubview(searchTextField)
        
        suggestionTableView.delegate = self;
        suggestionTableView.dataSource = self;
        suggestionTableView.frame = CGRectMake(0.0, (searchView.frame.origin.y + searchView.frame.size.height), SCREEN_WIDTH, SCREEN_HEIGHT - (searchView.frame.origin.y + searchView.frame.size.height) )
        self.view.addSubview(suggestionTableView)
        suggestionTableView.registerNib(UINib(nibName: "SuggestionTableViewCell", bundle: nil), forCellReuseIdentifier: SuggestionTableViewCellId )
        suggestionTableView.hidden = true
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return suggestionWeatherInfos == nil ? 0 : (suggestionWeatherInfos?.count)!
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(SuggestionTableViewCellId ) as? SuggestionTableViewCell
        
        let weatherInfo = suggestionWeatherInfos![indexPath.row] as! CD_WEATHER
        
        cell?.cityNameLabel.text = weatherInfo.cityName
        
        return cell!
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 45.0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let weatherInfo = suggestionWeatherInfos![indexPath.row] as! CD_WEATHER
        if Reachability.isConnectedToNetwork() == false {
            let viewController = WeatherResultViewController()
            viewController.weathers = LIST(cd_weather: weatherInfo)
            viewController.api_key = self.api_key
            self.navigationController?.pushViewController(viewController, animated: true)
        } else {
            searchTextField.text = weatherInfo.cityName
            searchTextField.delegate?.textFieldShouldReturn!(searchTextField)
        }
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        suggestionTableView.hidden = false
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        suggestionTableView.hidden = true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField.text?.characters.count > 0 {
            if Reachability.isConnectedToNetwork() == false {
                let cd_weather = DataManager.sharedInstance.getWeatherData(searchTextField.text!)
                if( cd_weather == nil ) {
                    let alertController = UIAlertController(title: "Alert", message: "It is Offline", preferredStyle: .Alert)
                    
                    let okayAction = UIAlertAction(title: "Okay", style: .Default) { (action) -> Void in
                        
                    }
                    alertController.addAction(okayAction)
                    dispatch_async(dispatch_get_main_queue(), {
                        self.navigationController?.presentViewController(alertController, animated: true, completion: {
                            
                        })
                    })
                } else {
                    let viewController = WeatherResultViewController()
                    viewController.weathers = LIST(cd_weather: cd_weather!)
                    viewController.api_key = self.api_key
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            } else {
                HttpRequest.requestGet(BASE_URL, targetUrl: RETREIVE_WEATHERLIST, success: { (response) -> Void in
                    let responseInDictionary = response as? NSDictionary
                    if responseInDictionary == nil {
                        
                    } else {
                        
                        self.weatherInfos = LIST(weather: responseInDictionary)
                        for weatherInfo in (self.weatherInfos!.datas! as NSArray as! [WEATHER]) {
                            if DataManager.sharedInstance.insertSearchData(weatherInfo) == true {
                                dispatch_async(dispatch_get_main_queue(), {
                                    self .retreiveData()
                                })
                                
                            }
                        }
                        
                        dispatch_async(dispatch_get_main_queue(), {
                            let viewController = WeatherResultViewController()
                            viewController.weathers = self.weatherInfos
                            viewController.api_key = self.api_key
                            self.navigationController?.pushViewController(viewController, animated: true)
                        })
                    }
                    
                    }, failure: { (error) -> Void in
                        if error.code == 903 {
                            let alertController = UIAlertController(title: "Alert", message: error.localizedDescription, preferredStyle: .Alert)
                            
                            let okayAction = UIAlertAction(title: "Okay", style: .Default) { (action) -> Void in
                                
                            }
                            alertController.addAction(okayAction)
                            dispatch_async(dispatch_get_main_queue(), {
                                self.navigationController?.presentViewController(alertController, animated: true, completion: {
                                    
                                })
                            })
                        }
                    }, parametersObjectsAndKeys: api_key!, "key", textField.text!, "q", "yes", "fx", "json", "format", "24", "tp", "1", "num_of_days")
            }
            
            textField.resignFirstResponder()
            return true
        } else {
            let viewController = WeatherResultViewController()
            self.navigationController?.pushViewController(viewController, animated: true)
            
            textField.resignFirstResponder()
            return true
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
