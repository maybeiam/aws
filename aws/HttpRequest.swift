//
//  HttpRequest.swift
//  aws
//
//  Created by Shepherd on 2016. 5. 15..
//  Copyright © 2016년 Shepherd. All rights reserved.
//

import UIKit

class HttpRequest: NSObject {
    class func requestGet(
        baseUrl : String?,
        targetUrl : String,
        success : ( (AnyObject) -> Void),
        failure : ( (NSError) -> Void),
        parametersObjectsAndKeys : String... ) {
        
        var absoluteUrl : String?
        if baseUrl == nil {
            absoluteUrl = BASE_URL
        }
        else {
            absoluteUrl = baseUrl
        }
        
        absoluteUrl! += targetUrl
        
        var parameters = [String]()
        for var i = 0; i < parametersObjectsAndKeys.count; i += 2 {
            var string = parametersObjectsAndKeys[i + 1]
            string += "="
            string += parametersObjectsAndKeys[i]
            parameters.append(string)
        }
        
        absoluteUrl! += "?"
        absoluteUrl! += parameters.joinWithSeparator("&")
        let customAllowedSet =  NSCharacterSet(charactersInString:/*"=\"#%/<>?@\\^`{|}"*/" ").invertedSet
        absoluteUrl = absoluteUrl!.stringByAddingPercentEncodingWithAllowedCharacters(customAllowedSet)
        
        let request = NSMutableURLRequest(URL: NSURL(string: absoluteUrl!)!)
        request.HTTPMethod = "GET"
        
        HttpRequest.sendRequest(request, success: { (result) -> Void in
            success( result )
        }) { (error) -> Void in
            failure( error )
        }
        
    }

    
    class func requestPost( baseUrl : String?,
                            targetUrl : String,
                            success : ( (AnyObject) -> Void),
                            failure : ( (NSError) -> Void),
                            parameters : NSMutableDictionary ) {
        
        
        var absoluteUrl : String?
        if baseUrl == nil {
            absoluteUrl = BASE_URL
        }
        else {
            absoluteUrl = ""
        }
        
        absoluteUrl! += targetUrl
        
        let request = NSMutableURLRequest(URL: NSURL(string: absoluteUrl!)!)
        request.HTTPMethod = "POST"
        
        var stringOnly = true
        for (_, value) in parameters {
            stringOnly = (value is String || value is NSString)
            if( stringOnly == false ) {
                break
            }
        }
        
        if stringOnly == true {
            var parameterArray = [String]()
            for (key,value) in parameters {
                //                    let notEncrypted = value as! String
                //                    let encrypted = AESManager.encrypt(notEncrypted)
                //                    println( "parameter : [\(notEncrypted)][\(encrypted)]" )
                parameterArray.append(String(format:"%@=%@", key as! String, value as! String ))
            }
            let joinedbody = parameterArray.joinWithSeparator("&")
            print( "body : [\(joinedbody)]"  )
            let encodedParameter = (parameterArray.joinWithSeparator("&") as NSString).dataUsingEncoding(NSUTF8StringEncoding)
            
            request.HTTPBody = encodedParameter
        }
        else {
            let boundary = "askldfhjkluilee-------------------"
            let contentType = String(format: "multipart/form-data; boundary=%@", boundary )
            request.setValue(contentType, forHTTPHeaderField: "Content-Type")
            
            let body = NSMutableData()
            
            for (key,value) in parameters {
                if value is UIImage {
                    body.appendData( (String( format:"--%@\r\n", boundary ) as NSString).dataUsingEncoding(NSUTF8StringEncoding)! )
                    body.appendData( (String( format:"Content-Disposition: form-data; name=\"%@\"; filename=\"a.jpg\"\r\n", key as! String ) as NSString).dataUsingEncoding(NSUTF8StringEncoding)! )
                    body.appendData( ("Content-Type: image/jpeg\r\n\r\n" as NSString).dataUsingEncoding(NSUTF8StringEncoding)! )
                    body.appendData(UIImageJPEGRepresentation( value as! UIImage, 0.5)!)
                    body.appendData( ("\r\n" as NSString).dataUsingEncoding(NSUTF8StringEncoding)! )
                }
                else if value is NSData {
                    
                }
                else if value is String {
                    body.appendData( (String( format:"--%@\r\n", boundary ) as NSString).dataUsingEncoding(NSUTF8StringEncoding)! )
                    body.appendData( (String( format:"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key as! String ) as NSString).stringByReplacingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!.dataUsingEncoding(NSUTF8StringEncoding)! )
                    body.appendData( (value as! NSString).dataUsingEncoding(NSUTF8StringEncoding)! )
                    body.appendData( ("\r\n" as NSString).dataUsingEncoding(NSUTF8StringEncoding)! )
                    let tempBody = NSMutableData()
                    tempBody.appendData( (String( format:"--%@\r\n", boundary ) as NSString).dataUsingEncoding(NSUTF8StringEncoding)! )
                    tempBody.appendData( (String( format:"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key as! String ) as NSString).dataUsingEncoding(NSUTF8StringEncoding)! )
                    tempBody.appendData( (value as! NSString).dataUsingEncoding(NSUTF8StringEncoding)! )
                    tempBody.appendData( ("\r\n" as NSString).dataUsingEncoding(NSUTF8StringEncoding)! )
                    print( "appended multipart as String : [\(String( data: tempBody, encoding: NSUTF8StringEncoding))]")
                } else if value is NSString {
                    let ns_value = value as! NSString
                    body.appendData( (String( format:"--%@\r\n", boundary ) as NSString).dataUsingEncoding(NSUTF8StringEncoding)! )
                    body.appendData( (String( format:"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key as! String ) as NSString).dataUsingEncoding(NSUTF8StringEncoding)! )
                    body.appendData( ns_value.dataUsingEncoding(NSUTF8StringEncoding)! )
                    body.appendData( ("\r\n" as NSString).dataUsingEncoding(NSUTF8StringEncoding)! )
                    let tempBody = NSMutableData()
                    tempBody.appendData( (String( format:"--%@\r\n", boundary ) as NSString).dataUsingEncoding(NSUTF8StringEncoding)! )
                    tempBody.appendData( (String( format:"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key as! String ) as NSString).dataUsingEncoding(NSUTF8StringEncoding)! )
                    tempBody.appendData( ns_value.dataUsingEncoding(NSUTF8StringEncoding)! )
                    tempBody.appendData( ("\r\n" as NSString).dataUsingEncoding(NSUTF8StringEncoding)! )
                    print( "appended multipart as NSString : [\(String( data: tempBody, encoding: NSUTF8StringEncoding))]")
                }
            }
            
            body.appendData( (String( format:"--%@--\r\n", boundary ) as NSString).dataUsingEncoding(NSUTF8StringEncoding)! )
            //                print( "multipart body : [\(String( data: body, encoding: NSUTF8StringEncoding))]")
            request.HTTPBody = body
        }
        
        
        HttpRequest.sendRequest(request, success: { (result) -> Void in
            success( result )
        }) { (error) -> Void in
            failure( error )
        }
    }
    
    class func requestImage( url : String, success : ( (UIImage) -> Void), failure : ( (NSError) -> Void) ) {
        
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        let request = NSMutableURLRequest(URL: NSURL(string: url)!)
        request.HTTPMethod = "GET"
        
        let task = session.dataTaskWithRequest(request, completionHandler: {(data : NSData?, response : NSURLResponse?, error : NSError?) in
            if error == nil {
                if data == nil {
                    failure( NSError(domain: "data is nil", code: 302, userInfo: nil) )
                }
                else {
                    let image = UIImage(data: data!)
                    success( image! )
                }
            }
            else {
                failure( error! )
            }
        });
        task.resume()
    }
    
    private class func sendRequest( request : NSMutableURLRequest, success : ( (AnyObject) -> Void), failure : ( (NSError) -> Void) ) {
        
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        let task = session.dataTaskWithRequest(request, completionHandler: {(data : NSData?, response : NSURLResponse?, error : NSError?) in
            var jsonParseError : NSError?
            if error == nil {
                if data == nil {
                    failure( NSError(domain: "data is nil", code: 302, userInfo: nil) )
                }
                else {
                    
                    let dictionary : NSDictionary = (try! NSJSONSerialization.JSONObjectWithData(data!, options : NSJSONReadingOptions.MutableContainers)) as! NSDictionary
                    
                    if( jsonParseError != nil ) {
                        failure( jsonParseError! )
                    }
                    else {
                        let data = dictionary.objectForKey("data") as? NSDictionary
                        if data?.objectForKey("error") != nil {
                            let errors = data?.objectForKey("error") as? NSArray
                            let errorDetail = errors?.objectAtIndex(0) as? NSDictionary
                            let errorMsg = errorDetail?.objectForKey("msg") as! String
                            
                            let details = NSMutableDictionary()
                            details.setValue(errorMsg, forKey: NSLocalizedDescriptionKey)
                            failure( NSError(domain: "Domain", code: 903, userInfo: details as [NSObject : AnyObject]) )
                        } else {
                            success( dictionary )
                        }
                        
                    }
                }
            }
            else {
            }
        });
        task.resume()
    }
}
