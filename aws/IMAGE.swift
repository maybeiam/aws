//
//  IMAGE.swift
//  aws
//
//  Created by Shepherd on 2016. 5. 15..
//  Copyright © 2016년 Shepherd. All rights reserved.
//

import UIKit

class IMAGE: NSObject {
    var url : String?
    var image : UIImage?
    init( url : String? ) {
        self.url = url;
    }
}
