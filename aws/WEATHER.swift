//
//  WEATHER.swift
//  aws
//
//  Created by Shepherd on 2016. 5. 15..
//  Copyright © 2016년 Shepherd. All rights reserved.
//

import UIKit

class WEATHER: NSObject {
    var humidity : String?
    var observation_time : String?
    var weatherDescriptions : NSArray?
    var weatherDesc : String? {
        return weatherDescriptions!.count > 0 ? weatherDescriptions?.objectAtIndex(0) as? String : ""
    }
    var weatherIconImages : NSArray?
    var weatherIconImage : IMAGE? {
        return weatherIconImages!.count > 0 ? weatherIconImages?.objectAtIndex(0) as? IMAGE : nil
    }
    var request : REQUEST?
    var city : String? {
        return request?.query
    }
    
    init( dictionary : NSDictionary ) {
        humidity = dictionary.objectForKey("humidity") as? String
        observation_time = dictionary.objectForKey("observation_time") as? String
        
        let descriptions = dictionary.objectForKey("weatherDesc") as? NSArray
        let processedDescriptions = NSMutableArray()
        for dic in (descriptions! as NSArray as! [NSDictionary]) {
            processedDescriptions.addObject( dic.objectForKey("value") as! String )
        }
        weatherDescriptions = NSArray(array: processedDescriptions)
        
        let images = dictionary.objectForKey("weatherIconUrl") as? NSArray
        let processedImages = NSMutableArray()
        for dic in (images! as NSArray as! [NSDictionary]) {
            processedImages.addObject( IMAGE(url: dic.objectForKey("value") as? String ) )
        }
        weatherIconImages = NSArray(array: processedImages)
    }
    
    init( cd_weather : CD_WEATHER ) {
        humidity = cd_weather.humidity
        observation_time = cd_weather.observationTime
        weatherDescriptions = NSArray(object: cd_weather.weatherDescription! )
        let iconImage = IMAGE(url: nil)
        if cd_weather.image != nil {
            iconImage.image = UIImage(data: cd_weather.image! )
            
        }
        weatherIconImages = NSArray(object: iconImage )
        request = REQUEST(cd_weather: cd_weather)
    }
}
