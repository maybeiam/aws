//
//  ViewController.swift
//  aws
//
//  Created by Shepherd on 2016. 5. 14..
//  Copyright © 2016년 Shepherd. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var mainTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

//        self.navigationController?.navigationBarHidden = true
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowAWSViewController" {
            let viewController = segue.destinationViewController as! AWSViewController
            viewController.api_key = "dd5bc7742928d1633213b5da075ec"
        } else if segue.identifier == "ShowChildAWSViewController" {
            let viewController = segue.destinationViewController as! ChildAWSViewController
            viewController.api_key = "f16c45e7217c8641fa4d5ae8e2415"
        }
    }

}

